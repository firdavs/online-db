package com.onlinedb.firdavs.onlindb;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Firdavs on 11.03.2016.
 */
public class MainActivity extends Activity {
    public static MainActivity _myLayout;
    public static EditText txt_login, txt_password;
    public static String user_login, user_password, user_code, server_host = "http://condor.alarstudios.com/test";
    public int cur_page = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        MainActivity._myLayout = this;

        Button btn_login = (Button) findViewById(R.id.btn_login);
        txt_login = (EditText) findViewById(R.id.edt_login);
        txt_password = (EditText) findViewById(R.id.edt_password);

        btn_login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

                if (txt_login.getText().toString().trim().length() == 0
                        || txt_password.getText().toString().trim().length() == 0) {
                    //show error auth message
                    generate_message(getString(R.string.app_warning), getString(R.string.app_fill_field));
                } else {
                    user_login = txt_login.getText().toString();
                    user_password = txt_password.getText().toString();

                    Url_Params params = new Url_Params(server_host + "/auth.cgi", user_login, user_password);
                    http_auth_me auth_me = new http_auth_me(MainActivity.this);
                    auth_me.execute(params);
                }
            }
        });
    }

    void generate_message(String title, String message) {
        AlertDialog.Builder alert_message = new AlertDialog.Builder(this);
        alert_message.setTitle(title);
        alert_message.setMessage(message);
        alert_message.setCancelable(false);
        alert_message.setNegativeButton(getString(R.string.app_ok), null); //ok
        alert_message.show();
    }

    public static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
