package com.onlinedb.firdavs.onlindb;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Firdavs on 11.03.2015.
 */
public class Parse_http_result {
    JSONArray _json_arr;
    JSONObject _json_obj;

    public void parse_result(Context ctx, String result, String json) {
        if(result.equalsIgnoreCase("auth_ok")){
            try {
                _json_obj = new JSONObject(json);
                MainActivity._myLayout.user_code = _json_obj.get("code").toString();
            } catch (Exception e) {
            }
            Toast.makeText(ctx, ctx.getString(R.string.app_login_ok), Toast.LENGTH_LONG).show();

            //get first page
            Url_Params params = new Url_Params(MainActivity._myLayout.server_host + "/data.cgi", String.valueOf(MainActivity._myLayout.cur_page), MainActivity._myLayout.user_code);
            http_records_me records_me = new http_records_me(ctx);
            records_me.execute(params);

        }else if (result.equalsIgnoreCase("db_records")) { //
            try {
                _json_arr = new JSONArray(json);
            } catch (Exception e) {
            }

            Intent intent = null;
            if(ShowRecords._myLayout == null) {
                intent = new Intent(ctx, ShowRecords.class);
            }


            ShowRecords._myLayout.rec_id = new String[_json_arr.length()];
            ShowRecords._myLayout.rec_name = new String[_json_arr.length()];
            ShowRecords._myLayout.rec_email = new String[_json_arr.length()];
            ShowRecords._myLayout.rec_lat = new String[_json_arr.length()];
            ShowRecords._myLayout.rec_lon = new String[_json_arr.length()];
            ShowRecords._myLayout.rec_img = new String[_json_arr.length()];
            //
            for (int i = 0; i < _json_arr.length(); i++) {
                try {
                    _json_obj = _json_arr.getJSONObject(i);
                    ShowRecords._myLayout.rec_id[i] = _json_obj.get("id").toString();
                    ShowRecords._myLayout.rec_name[i] = _json_obj.get("name").toString();
                    ShowRecords._myLayout.rec_email[i] = _json_obj.get("email").toString();
                    ShowRecords._myLayout.rec_lat[i] = _json_obj.get("lat").toString();
                    ShowRecords._myLayout.rec_lon[i] = _json_obj.get("lon").toString();
                    ShowRecords._myLayout.rec_img[i] = _json_obj.get("image").toString();;
                } catch (Exception e) {
                }
            }

            if(ShowRecords._myLayout == null) {
                ctx.startActivity(intent);
            }else{
                ShowRecords._myLayout.mAdapter.notifyDataSetInvalidated();
            }
        }
    }
}
