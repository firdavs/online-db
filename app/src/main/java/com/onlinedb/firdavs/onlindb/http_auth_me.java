package com.onlinedb.firdavs.onlindb;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Firdavs on 11.03.2016.
 */
public class http_auth_me extends AsyncTask<Url_Params, Void, Void> {
    StringBuilder builder = new StringBuilder();
    Boolean _result = false;
    ProgressDialog progressdlg;
    private Context context;
    JSONObject jsonObjRecv = null;

    public http_auth_me(Context cxt) {
        context = cxt;
        progressdlg = new ProgressDialog(context);
    }

    //@Override
    protected Void doInBackground(Url_Params... params) {
        String url = params[0].url;
        String username = params[0].param1;
        String password= params[0].param2;
        try {
            DefaultHttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPostRequest = new HttpPost(url);

            List<NameValuePair> data = new ArrayList<NameValuePair>();
            data.add(new BasicNameValuePair("username", username));
            data.add(new BasicNameValuePair("password", password));
            httpPostRequest.setEntity(new UrlEncodedFormEntity(data, "UTF-8"));

            HttpResponse response = httpclient.execute(httpPostRequest);
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                InputStream instream = entity.getContent();
                String resultString = MainActivity._myLayout.convertStreamToString(instream);
                instream.close();

                jsonObjRecv = new JSONObject(resultString);

                if (jsonObjRecv != null) {
                    try {
                        if (jsonObjRecv.get("status").toString().equalsIgnoreCase("ok")) {
                            _result = true;
                        } else {
                            _result = false;
                        }
                    } catch (Exception e) {
                        _result = false;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        showDialog(context);
    }

    @Override
    protected void onPostExecute(Void result) {
        if (_result == true) {
            Parse_http_result _prs = new Parse_http_result();
            _prs.parse_result(context, "auth_ok", jsonObjRecv.toString());

        } else {
            Toast.makeText(this.context, context.getString(R.string.app_login_failes), Toast.LENGTH_LONG).show();
        }
        closeDialog();
    }

    private void closeDialog() {
        if (progressdlg != null) {
            progressdlg.dismiss();
        }
    }

    private void showDialog(Context context) {
        if (progressdlg != null) {
            try {
                progressdlg.dismiss();
                progressdlg.cancel();
                progressdlg = null;
            } catch (Exception e) {
            }
        }
        if (progressdlg == null) {
            progressdlg = new ProgressDialog(context);
            progressdlg.setMessage(context.getString(R.string.app_progress_auth));
            try {
                progressdlg.show();
            } catch (Exception e) {
            }
        }

    }
}
