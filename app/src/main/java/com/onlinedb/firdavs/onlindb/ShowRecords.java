package com.onlinedb.firdavs.onlindb;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by Firdavs on 11.03.2016.
 */
public class ShowRecords extends ListActivity {
    myAdapter mAdapter;
    public static ShowRecords _myLayout;
    public static String[] rec_id, rec_name, rec_email, rec_lat, rec_lon, rec_img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_db_records);
        ShowRecords._myLayout = this;

        Button btn_prev = (Button) findViewById(R.id.btn_prev);
        Button btn_next = (Button) findViewById(R.id.btn_next);


        mAdapter = new myAdapter(this);
        setListAdapter(mAdapter);

        btn_next.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                //next page
                if(MainActivity._myLayout.cur_page < 10) {
                    MainActivity._myLayout.cur_page++;
                    get_db_records();
                }
            }
        });

        btn_prev.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                //prev page
                if(MainActivity._myLayout.cur_page > 1) {
                    MainActivity._myLayout.cur_page--;
                    get_db_records();
                }
            }
        });

    }

    private void get_db_records(){
        Url_Params params = new Url_Params(MainActivity._myLayout.server_host + "/data.cgi", String.valueOf(MainActivity._myLayout.cur_page), MainActivity._myLayout.user_code);
        http_records_me records_me = new http_records_me(ShowRecords.this);
        records_me.execute(params);
    }

    public void onListItemClick (ListView parent, View v, int position, long id) {
        show_geo_position(rec_lat[position], rec_lon[position], rec_id[position], rec_name[position], rec_email[position]);
    }

    void show_geo_position(String lat, String lon, String id, String name, String email)
    {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("geo:0,0?q=" + lat + "," + lon + "(ID = " + id + ",Name = " + name + ", Email = " + email + ")"));
        try {
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class myAdapter extends BaseAdapter {
        private LayoutInflater mLayoutInflater;

        public myAdapter(Context ctx) {
            mLayoutInflater = LayoutInflater.from(ctx);
        }

        public int getCount() {
            return rec_id.length;
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }


        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null)
                convertView = mLayoutInflater.inflate(R.layout.list_db_rows, null);

            TextView txt_rec_id = (TextView) convertView.findViewById(R.id.rec_id);
            txt_rec_id.setText(rec_id[position]);

            TextView txt_rec_name = (TextView) convertView.findViewById(R.id.rec_name);
            txt_rec_name.setText(rec_name[position]);

            TextView txt_rec_email = (TextView) convertView.findViewById(R.id.rec_email);
            txt_rec_email.setText(rec_email[position]);

            TextView txt_rec_lat = (TextView) convertView.findViewById(R.id.rec_lat);
            txt_rec_lat.setText(rec_lat[position]);

            TextView txt_rec_lon = (TextView) convertView.findViewById(R.id.rec_lon);
            txt_rec_lon.setText(rec_lon[position]);

            ImageView vw_rec_img = (ImageView) convertView.findViewById(R.id.rec_img);
            http_get_image down_img = new http_get_image(vw_rec_img);
            down_img.execute(rec_img[position]);
//            vw_rec_img.setImageResource(rec_img[position]);

            return convertView;
        }
    }
}
